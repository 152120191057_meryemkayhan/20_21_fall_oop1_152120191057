#include<iostream>
#include<stdlib.h>
#include<fstream>
using namespace std;
//! the initial size of the array 
#define size 50   
//! the function that will sum the members of the array 
int Sum(int[],int); 
//! the function that will compute the product of the members of the array 
int Product(int[], int);
//! the function that computes the average value of the array 
float Average(int[], int);
//! the function that finds the smallest member of the array 
int Smallest(int[], int);
int main()
{
	fstream input_file;
	int i=0, array[size];
	//! the size of the array that will be taken from the input file 
	int nr = 0; 

	//! opening the file for read only
	input_file.open("input.txt", ios::in);  

	if (input_file.is_open())
	{//! checking if the file exists
		cout << "The file opened succesfully.\n\n";  
		input_file >> nr;

		while (!input_file.eof())
		{//! getting the members of the array from the file
			input_file >> array[i];  
			i++;
		}
		//! checking if the number taken from the file actually equals to the array size
		if (i > nr) 
			cout << "ERROR! The size of the array is too small!";
		else if (i < nr)
			cout << "ERROR! The size of the array is too big!";
		else
		{
			cout << "The array of numbers:\n";
			for (i = 0; i < nr; i++)
				cout << array[i] << "\t";

			cout << endl << endl;
			cout << "Sum is " << Sum(array, nr) << endl;
			cout << "Product is " << Product(array, nr) << endl;
			cout << "Average is " << (float)Average(array, nr) << endl;
			cout << "Smallest is " << Smallest(array, nr) << endl;
		}
	}
		
	else
		cout << "Error opening file.\n";
	//! closing the file
	input_file.close();  
	cout << endl << endl;
	system("pause");
	return 0;
}
int Sum(int a[], int number)
{
	int i, sum=0;

	for (i = 0; i < number; i++)
		sum = sum + a[i];
	
	return sum;
}
int Product(int a[], int number)
{
	int i, product = 1;

	for (i = 0; i < number; i++)
		product = product * a[i];

	return product;
}
float Average(int a[], int number)
{
	int sum = 0;
	float avg = 0;
	//! calling the Sum function to help us find the average value 
	sum = Sum(a, number);
	avg = (float)sum / number;

	return avg;
}
int Smallest(int a[], int number)
{
	int i, j, smallest = 0;
	//! the first member of the array is assumed the smallest 
	smallest = a[0];
	for (i = 1; i < number; i++)
	{//! checking if the next member of the array is smaller than our smallest value
		if (smallest > a[i])
			smallest = a[i];
	}
	return smallest;
}