#include <iostream>
#include <stdlib.h>
using namespace std;

void update(int *a, int *b)
{
	int x = *a, y = *b;
	*a = x + y;
	if (x>y) *b = x - y;
	if (x<y) *b = y - x;
}

int main() {
	int a, b;
	int *pa = &a, *pb = &b;

	cin >> a >> b;
	update(pa, pb);

	cout << a << endl << b;

	system("pause");
	return 0;
}