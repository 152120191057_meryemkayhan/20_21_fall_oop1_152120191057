#include <iostream>
#include <cstdio>
#include<stdlib.h>
#include<iomanip>
using namespace std;

int main() {

	int a;
	long int b;
	char ch = ' ';
	float f;
	double d;

	cin >> a >> b >> ch >> f >> d;
	cout << a << endl;
	cout << b << endl;
	cout << ch << endl;
	cout << fixed << setprecision(3) << f << endl;
	cout << fixed << setprecision(9) << d << endl;

	system("pause");
	return 0;
}