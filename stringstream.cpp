#include <sstream>
#include <vector>
#include <iostream>
#include<stdlib.h>
using namespace std;

vector<int> parseInts(string str) {
	stringstream ss(str);
	char tmp;
	int a;
	vector<int> v;

	while (ss >> a)
	{
		v.push_back(a);
		ss >> tmp;
	}
	return v;
}

int main() {
	string str;
	cin >> str;
	vector<int> integers = parseInts(str);
	for (int i = 0; i < integers.size(); i++) {
		cout << integers[i] << "\n";
	}


	system("pause");
	return 0;
}