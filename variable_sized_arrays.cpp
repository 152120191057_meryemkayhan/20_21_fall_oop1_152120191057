#include <iostream>
#include <stdlib.h>
using namespace std;


int main() {
	int k, q, i, j;
	cin >> k >> q;

	int **a = new int*[k];

	for (i = 0; i<k; i++)
	{
		int size;
		cin >> size;
		a[i] = new int[size];
		for (j = 0; j<size; j++)
		{
			cin >> a[i][j];
		}
	}

	for (i = 0; i<q; i++)
	{
		int x, y;
		cin >> x >> y;
		cout << a[x][y] << endl;
	}

	system("pause");
	return 0;
}